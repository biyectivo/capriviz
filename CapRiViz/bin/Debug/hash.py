import hashlib
import sys
import io
from os.path import exists

# Read parameters
filename_config = "C:\\Users\\biyec\\OneDrive\\Escritorio\\example_config_v1.xlsx"
filename_baremos = "C:\\Users\\biyec\\OneDrive\\Escritorio\\example_baremos_v1.xlsx"

nargs = len(sys.argv)
if (nargs >= 2):
    filename_config = sys.argv[1]
if (nargs >= 3):
    filename_baremos = sys.argv[2]
    
BLOCKSIZE = 65536

text = ""
if exists(filename_config):
    hasher = hashlib.sha1()
    with open(filename_config, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    text = hasher.hexdigest()+"\n"
else:
    text = "0"+"\n"
    
text2 = ""
if exists(filename_baremos):    
    hasher2 = hashlib.sha1()
    with open(filename_baremos, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher2.update(buf)
            buf = afile.read(BLOCKSIZE)
    text2 = hasher2.hexdigest()+"\n"

file = io.open("hashes.dat", "w", encoding="utf-8")
file.write(text)
file.write(text2)
file.close()
