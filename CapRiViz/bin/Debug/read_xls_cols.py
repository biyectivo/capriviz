# Libraries
from openpyxl import load_workbook
import sys
import io

# Read parameters
filename_config = "C:\\Users\\biyec\\OneDrive\\Escritorio\\example_config_v1.xlsx"
sheet_name_config = "ANSI_cat_cpr_respaldo_configura"
filename_baremos = "C:\\Users\\biyec\\OneDrive\\Escritorio\\example_baremos_v1.xlsx"
sheet_name_baremos = "ANSI_cat_cpr_baremos"

nargs = len(sys.argv)
if (nargs >= 2):
    filename_config = sys.argv[1]
if (nargs >= 3):
    sheet_name_config = sys.argv[2]
if (nargs >= 4):
    filename_baremos = sys.argv[3]
if (nargs >= 5):
    sheet_name_baremos = sys.argv[4]
    


# Open config and baremos files to read columns
workbook_config = load_workbook(filename_config, read_only=False, keep_vba=False, data_only=True, keep_links=False)
workbook_baremos = load_workbook(filename_baremos, read_only=False, keep_vba=False, data_only=True, keep_links=False)

sheet_config = workbook_config[sheet_name_config]
sheet_baremos = workbook_baremos[sheet_name_baremos]

columns_config = []
columns_baremos = []

for col in sheet_config.iter_cols(min_col=1, max_col=sheet_config.max_column, min_row=1, max_row=1, values_only=True):
    for cell in col:
        columns_config.append(cell)

for col in sheet_baremos.iter_cols(min_col=1, max_col=sheet_baremos.max_column, min_row=1, max_row=1, values_only=True):
    for cell in col:
        columns_baremos.append(cell)


# Write columns to DAT files
file_col_config = io.open("config_columns.dat", "w", encoding="utf-8")
for col in columns_config:
    file_col_config.write(str(col)+"\n")
file_col_config.close()


file_col_baremos = io.open("baremos_columns.dat", "w", encoding="utf-8")
for col in columns_baremos:
    file_col_baremos.write(str(col)+"\n")
file_col_baremos.close()

workbook_config.close()
workbook_baremos.close()