# Libraries
from openpyxl import load_workbook
import sys
import io
import re

# Read parameters
filename_config = "C:\\Users\\biyec\\OneDrive\\Escritorio\\example_config_v1.xlsx"
sheet_name_config = "ANSI_cat_cpr_respaldo_configura"
filename_baremos = "C:\\Users\\biyec\\OneDrive\\Escritorio\\example_baremos_v1.xlsx"
sheet_name_baremos = "ANSI_cat_cpr_baremos"
selected_cols_config_string = "ejecucion,query,tema"
selected_cols_baremos_string = "procesos,regla,valor_destino,valor_origen,campo_eval,campo_objetivo"
parametros = "$PER=MES"

nargs = len(sys.argv)
if (nargs >= 2):
    filename_config = sys.argv[1]
if (nargs >= 3):
    sheet_name_config = sys.argv[2]
if (nargs >= 4):
    selected_cols_config_string = sys.argv[3]
if (nargs >= 5):
    filename_baremos = sys.argv[4]
if (nargs >= 6):
    sheet_name_baremos = sys.argv[5]
if (nargs >= 7):
    selected_cols_baremos_string = sys.argv[6]
if (nargs >= 8):
    parametros = sys.argv[7]

selected_cols_config = selected_cols_config_string.split(",")
selected_cols_baremos = selected_cols_baremos_string.split(",")


'''
print(filename_config)
print(sheet_name_config)
print(selected_cols_config)
print(filename_baremos)
print(sheet_name_baremos)
print(selected_cols_baremos)
print(parametros)
'''
print("Python process_rules.py v1 JABV - Processing start")

# Open config and baremos files to read columns
workbook_config = load_workbook(filename_config, read_only=False, keep_vba=False, data_only=True, keep_links=False)
workbook_baremos = load_workbook(filename_baremos, read_only=False, keep_vba=False, data_only=True, keep_links=False)

sheet_config = workbook_config[sheet_name_config]
sheet_baremos = workbook_baremos[sheet_name_baremos]

columns_config = []
columns_baremos = []

# Read columns and check indices of columns
for col in sheet_config.iter_cols(min_row=1, max_row=1, values_only=True):
    for cell in col:
        columns_config.append(cell)
for col in sheet_baremos.iter_cols(min_row=1, max_row=1, values_only=True):
    for cell in col:
        columns_baremos.append(cell)



queries = {}


# Load rules into dictionary
for row in sheet_config.iter_rows(min_row=2, values_only=True):
    if (row[0] != None):
        process_id = str(row[columns_config.index(selected_cols_config[0])])
        queries[process_id] = {}
        queries[process_id]["query"] = str(row[columns_config.index(selected_cols_config[1])])
        queries[process_id]["tabla"] = str(row[columns_config.index(selected_cols_config[2])]).split("|")[1]        
        queries[process_id]["baremosreglas"] = []
#print(queries)
    
case_statement = "(CASE "
    
for row in sheet_baremos.iter_rows(min_row=2, values_only=True):
    tipo = str(row[columns_baremos.index(selected_cols_baremos[1])])
    valor_baremo_condicion = str(row[columns_baremos.index(selected_cols_baremos[2])]) if row[columns_baremos.index(selected_cols_baremos[2])] != None else ""
    nombre_campo_origen = str(row[columns_baremos.index(selected_cols_baremos[4])])
    nombre_campo_destino = str(row[columns_baremos.index(selected_cols_baremos[5])])
    valor_campo_origen = str(row[columns_baremos.index(selected_cols_baremos[3])]) if row[columns_baremos.index(selected_cols_baremos[3])] != None else ""
    
    applicable_processes = str(row[columns_baremos.index(selected_cols_baremos[0])]).split("@")

    if (tipo in ["condicion", "calculado"]):
        for process in applicable_processes:
            if (process in queries):
                queries[process]["baremosreglas"].append(valor_baremo_condicion + " AS " + nombre_campo_destino)
                # print("Agregando [ "+valor_baremo_condicion + " AS " + nombre_campo_destino+" ] a " + process)
    elif (valor_campo_origen == "default"):
        for process in applicable_processes:
            if (process in queries):
                # print("Revisar si agregar baremo a " + process)
                if (case_statement != "(CASE "):
                    queries[process]["baremosreglas"].append(case_statement + " ELSE '" + valor_baremo_condicion + "' END) AS " + nombre_campo_destino)                    
                    # print("Agregando [ "+case_statement+ " ELSE '" + valor_baremo_condicion + "' END) AS " + nombre_campo_destino+" ] a " + process)
        case_statement = "(CASE "
    else:
        case_statement = case_statement + " WHEN " + nombre_campo_origen + " = '" + valor_campo_origen + "' THEN '" + valor_baremo_condicion + "'"
        # print("  Construyendo case statement con "+" WHEN " + nombre_campo_origen + " = '" + valor_campo_origen + "' THEN '" + valor_baremo_condicion + "'" )

# print(queries)

parametrosList = parametros.split(",")
# Store parsed queries to file
file = io.open("parsed_queries.dat", "w", encoding="utf-8")
for process in queries:
    parsed_query = "CREATE TABLE "+queries[process]["tabla"]+" AS "+queries[process]["query"]
    # Reemplazar baremosreglas
    if len(queries[process]["baremosreglas"]) == 0:
        parsed_query = re.sub(",[ ]*\$BAREMOSREGLAS", "", parsed_query)
    else:
        parsed_query = parsed_query.replace("$BAREMOSREGLAS", ",".join(queries[process]["baremosreglas"]))
    
    # Replace with parameter tags
    for paramequal in parametrosList:
        param = paramequal.split("=")
        parsed_query = parsed_query.replace(param[0], param[1])
    
    parsed_query = parsed_query+";\n\n"
    file.write(parsed_query)
file.close()

print("Python process_rules.py v1 JABV - Processing end")
workbook_config.close()
workbook_baremos.close()