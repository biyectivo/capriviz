﻿Imports Microsoft.Office.Interop

Public Class frmMain

    Protected reglasColumnas As New List(Of String)
    Protected baremosColumnas As New List(Of String)

    Private Sub checkHashes()
        Dim pythonPath As String = rutaPythonTextBox.Text
        Dim pythonProgramPath As String = My.Computer.FileSystem.CurrentDirectory
        Dim pythonProgram As String = "hash.py"
        Dim fileReader As System.IO.StreamReader

        Me.Cursor = Cursors.WaitCursor
        Dim reglasDisplayHash As Boolean = False
        Dim baremosDisplayHash As Boolean = False
        Dim reglasHash As String = ""
        Dim baremosHash As String = ""


        ' Hashes using Python
        Shell(pythonPath + "\python.exe " + Chr(34) + pythonProgramPath + "\" + pythonProgram + Chr(34) + " " +
              Chr(34) + reglasTextBox.Text + Chr(34) + " " +
              Chr(34) + baremosTextBox.Text + Chr(34),
              AppWinStyle.NormalFocus, True, 1000 * 3)


        fileReader = My.Computer.FileSystem.OpenTextFileReader(pythonProgramPath + "\hashes.dat")
        While (Not fileReader.EndOfStream)
            reglasHash = fileReader.ReadLine()
            baremosHash = fileReader.ReadLine()
        End While
        fileReader.Close()

        If reglasTextBox.Text = "" Then
            reglasHashLabel.Text = ""
        ElseIf Not FileIO.FileSystem.FileExists(reglasTextBox.Text) Then
            reglasHashLabel.Text = "El archivo no existe"
        Else
            reglasDisplayHash = True
        End If

        If baremosTextBox.Text = "" Then
            baremosHashLabel.Text = ""
        ElseIf Not FileIO.FileSystem.FileExists(baremosTextBox.Text) Then
            baremosHashLabel.Text = "El archivo no existe"
        Else
            baremosDisplayHash = True
        End If

        If reglasDisplayHash And reglasHash <> "0" Then
            reglasHashLabel.Text = reglasHash
        End If

        If baremosDisplayHash And baremosHash <> "0" Then
            baremosHashLabel.Text = baremosHash
        End If

        getColumnsButton.Enabled = FileIO.FileSystem.FileExists(reglasTextBox.Text) AndAlso FileIO.FileSystem.FileExists(baremosTextBox.Text)

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub reglasButton_Click(sender As Object, e As EventArgs) Handles reglasButton.Click
        Dim result = reglasFileDialog.ShowDialog()
        If result = Windows.Forms.DialogResult.OK Then
            If My.Computer.FileSystem.FileExists(reglasFileDialog.FileName) Then
                reglasTextBox.Text = reglasFileDialog.FileName
            End If
        End If

        checkHashes()
    End Sub

    Private Sub baremosButton_Click(sender As Object, e As EventArgs) Handles baremosButton.Click
        Dim result = baremosFileDialog.ShowDialog()
        If result = Windows.Forms.DialogResult.OK Then
            If My.Computer.FileSystem.FileExists(baremosFileDialog.FileName) Then
                baremosTextBox.Text = baremosFileDialog.FileName
            End If
        End If

        checkHashes()
    End Sub

    Private Sub getColumnsButton_Click(sender As Object, e As EventArgs) Handles getColumnsButton.Click
        Dim pythonPath As String = rutaPythonTextBox.Text
        Dim pythonProgramPath As String = "."
        Dim pythonProgram As String = "read_xls_cols.py"
        Dim sheet_config As String = sheetReglasTextBox.Text
        Dim sheet_baremos As String = sheetBaremosTextBox.Text
        Dim fileReader As System.IO.StreamReader

        Me.Cursor = Cursors.WaitCursor

        ' Read columns, using Python
        Shell(pythonPath + "\python.exe " + pythonProgramPath + "\" + pythonProgram + " " + reglasTextBox.Text + " " + sheet_config + " " + baremosTextBox.Text + " " + sheet_baremos, AppWinStyle.Hide, True, 1000 * 1)


        reglasColumnas.Clear()
        baremosColumnas.Clear()

        fileReader = My.Computer.FileSystem.OpenTextFileReader(pythonProgramPath + "\config_columns.dat", System.Text.Encoding.UTF8)
        While (Not fileReader.EndOfStream)
            reglasColumnas.Add(fileReader.ReadLine())
        End While
        fileReader.Close()

        fileReader = My.Computer.FileSystem.OpenTextFileReader(pythonProgramPath + "\baremos_columns.dat", System.Text.Encoding.UTF8)
        While (Not fileReader.EndOfStream)
            baremosColumnas.Add(fileReader.ReadLine())
        End While
        fileReader.Close()

        ' Clear datagrid
        reglasDataView.Rows.Clear()
        baremosDataView.Rows.Clear()

        If reglasDataView.Columns.Count > 1 Then
            reglasDataView.Columns.RemoveAt(1)
        End If

        If baremosDataView.Columns.Count > 1 Then
            baremosDataView.Columns.RemoveAt(1)
        End If

        logTextBox.Text = ""



        ' Add column and rows

        Dim cmb As New DataGridViewComboBoxColumn()
        cmb.HeaderText = "Columna"
        cmb.Name = "Columna"

        For Each col In reglasColumnas
            cmb.Items.Add(col)
        Next

        reglasDataView.Columns.Add(cmb)

        With (reglasDataView)
            .Rows.Add({"ID de proceso"})
            .Rows.Add({"Query"})
            .Rows.Add({"Tabla destino"})
        End With

        cmb = New DataGridViewComboBoxColumn()
        cmb.HeaderText = "Columna"
        cmb.Name = "Columna"

        For Each col In baremosColumnas
            cmb.Items.Add(col)
        Next

        baremosDataView.Columns.Add(cmb)

        With (baremosDataView)
            .Rows.Add({"IDs de procesos"})
            .Rows.Add({"Tipo de registro"})
            .Rows.Add({"Valor destino"})
            .Rows.Add({"Valor origen (baremos)"})
            .Rows.Add({"Campo origen (baremos)"})
            .Rows.Add({"Campo destino"})
        End With

        ' Set default values to combos
        If reglasColumnas.Contains("ejecucion") Then
            reglasDataView.Rows(0).Cells(1).Value = "ejecucion"
        End If
        If reglasColumnas.Contains("query") Then
            reglasDataView.Rows(1).Cells(1).Value = "query"
        End If
        If reglasColumnas.Contains("tema") Then
            reglasDataView.Rows(2).Cells(1).Value = "tema"
        End If

        If baremosColumnas.Contains("procesos") Then
            baremosDataView.Rows(0).Cells(1).Value = "procesos"
        End If
        If baremosColumnas.Contains("regla") Then
            baremosDataView.Rows(1).Cells(1).Value = "regla"
        End If
        If baremosColumnas.Contains("valor_destino") Then
            baremosDataView.Rows(2).Cells(1).Value = "valor_destino"
        End If
        If baremosColumnas.Contains("valor_origen") Then
            baremosDataView.Rows(3).Cells(1).Value = "valor_origen"
        End If
        If baremosColumnas.Contains("campo_eval") Then
            baremosDataView.Rows(4).Cells(1).Value = "campo_eval"
        End If
        If baremosColumnas.Contains("campo_objetivo") Then
            baremosDataView.Rows(5).Cells(1).Value = "campo_objetivo"
        End If

        Me.Cursor = Cursors.Default

        GroupBox2.Visible = True
        procesarButton.Visible = True
    End Sub


    Private Sub reglasTextBox_TextChanged(sender As Object, e As EventArgs) Handles reglasTextBox.TextChanged
        checkHashes()
    End Sub

    Private Sub baremosTextBox_TextChanged(sender As Object, e As EventArgs) Handles baremosTextBox.TextChanged
        checkHashes()
    End Sub

    Private Sub procesarButton_Click(sender As Object, e As EventArgs) Handles procesarButton.Click
        Dim pythonPath As String = rutaPythonTextBox.Text
        Dim pythonProgramPath As String = My.Computer.FileSystem.CurrentDirectory
        Dim pythonProgram As String = "process_rules.py"
        Dim sheet_config As String = sheetReglasTextBox.Text
        Dim sheet_baremos As String = sheetBaremosTextBox.Text

        Dim reglasCamposSelected As New List(Of String)
        Dim baremosCamposSelected As New List(Of String)
        Dim reglasColumnasString As String
        Dim baremosColumnasString As String

        Me.Cursor = Cursors.WaitCursor

        TabControl1.SelectedTab = TabControl1.TabPages(1)

        For Each caract In reglasDataView.Rows
            reglasCamposSelected.Add(caract.cells(1).value)
        Next
        For Each caract In baremosDataView.Rows
            baremosCamposSelected.Add(caract.cells(1).value)
        Next
        reglasColumnasString = String.Join(",", reglasCamposSelected.ToArray())
        baremosColumnasString = String.Join(",", baremosCamposSelected.ToArray())

        Dim parametros As String = ""

        For Each row In parametrosMotorDataGrid.Rows
            parametros = parametros + row.cells(0).value + "=" + row.cells(1).value + ","
        Next

        parametros = Strings.Left(parametros, Len(parametros) - 1)

        logTextBox.Text = "*** Iniciando procesamiento de reglas a las " + DateTime.Now.ToString("G") + " ***" + vbNewLine
        logTextBox.Text += " Fichero de reglas: " + reglasTextBox.Text + " (" + reglasHashLabel.Text + ")" + vbNewLine
        logTextBox.Text += " Fichero de baremos: " + baremosTextBox.Text + " (" + baremosHashLabel.Text + ")" + vbNewLine
        logTextBox.Text += "------------------------------------------------------------------------------------------------------------------------" + vbNewLine

        Dim programString As String
        Dim argumentString As String

        programString = pythonPath + "\python.exe"
        argumentString = Chr(34) + pythonProgramPath + "\" + pythonProgram + Chr(34) + " " +
                    Chr(34) + reglasTextBox.Text + Chr(34) + " " +
                    Chr(34) + sheet_config + Chr(34) + " " +
                    Chr(34) + reglasColumnasString + Chr(34) + " " +
                    Chr(34) + baremosTextBox.Text + Chr(34) + " " +
                    Chr(34) + sheet_baremos + Chr(34) + " " +
                    Chr(34) + baremosColumnasString + Chr(34) + " " +
                    Chr(34) + parametros + Chr(34)
        logTextBox.Text += "Python: " + programString + vbNewLine
        logTextBox.Text += "Arguments: " + argumentString + vbNewLine

        Using process As New Process()
            process.StartInfo.FileName = programString
            process.StartInfo.Arguments = argumentString

            process.StartInfo.UseShellExecute = False
            process.StartInfo.RedirectStandardOutput = True
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            process.Start()

            Dim reader As IO.StreamReader = process.StandardOutput
            Dim output As String = reader.ReadToEnd()

            logTextBox.Text = logTextBox.Text + output + vbNewLine
            'process.WaitForExit()
        End Using


        logTextBox.Text += "------------------------------------------------------------------------------------------------------------------------" + vbNewLine
        logTextBox.Text += vbNewLine + "*** Terminado a las " + DateTime.Now.ToString("G") + " ***"
        Me.Cursor = Cursors.Default

        GroupBox2.Visible = False
        procesarButton.Visible = False
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim linea As String = ""
        Dim componentes() As String

        Dim filereader = My.Computer.FileSystem.OpenTextFileReader("config.dat")
        While (Not fileReader.EndOfStream)
            linea = fileReader.ReadLine()
            componentes = linea.Split("=")
            If Strings.Left(componentes(0), 1) = "$" Then
                parametrosMotorDataGrid.Rows.Add(componentes(0), componentes(1))
            ElseIf componentes(0) = "ruta_python" Then
                rutaPythonTextBox.Text = componentes(1)
            ElseIf componentes(0) = "nombre_hoja_parametria" Then
                sheetReglasTextBox.Text = componentes(1)
            ElseIf componentes(0) = "nombre_hoja_baremos" Then
                sheetBaremosTextBox.Text = componentes(1)
            End If
        End While
        fileReader.Close()

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim file = My.Computer.FileSystem.OpenTextFileWriter("config.dat", False)
        file.WriteLine("ruta_python=" + rutaPythonTextBox.Text)
        file.WriteLine("nombre_hoja_parametria=" + sheetReglasTextBox.Text)
        file.WriteLine("nombre_hoja_baremos=" + sheetBaremosTextBox.Text)

        For Each row In parametrosMotorDataGrid.Rows
            file.WriteLine(row.cells(0).value + "=" + row.cells(1).value)
        Next
        file.Close()
    End Sub
End Class
