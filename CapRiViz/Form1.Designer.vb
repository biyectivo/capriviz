﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.procesarButton = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.baremosDataView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.reglasDataView = New System.Windows.Forms.DataGridView()
        Me.Característica = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.baremosHashLabel = New System.Windows.Forms.Label()
        Me.reglasHashLabel = New System.Windows.Forms.Label()
        Me.sheetBaremosTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.sheetReglasTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.getColumnsButton = New System.Windows.Forms.Button()
        Me.baremosButton = New System.Windows.Forms.Button()
        Me.reglasButton = New System.Windows.Forms.Button()
        Me.baremosTextBox = New System.Windows.Forms.TextBox()
        Me.reglasTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.logTextBox = New System.Windows.Forms.RichTextBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.parametrosMotorDataGrid = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Valor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.rutaPythonTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.reglasFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.baremosFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.baremosDataView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.reglasDataView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.parametrosMotorDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(957, 617)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.procesarButton)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(949, 591)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Procesar parametría"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'procesarButton
        '
        Me.procesarButton.Location = New System.Drawing.Point(18, 540)
        Me.procesarButton.Name = "procesarButton"
        Me.procesarButton.Size = New System.Drawing.Size(914, 32)
        Me.procesarButton.TabIndex = 15
        Me.procesarButton.Text = "Procesar parametría"
        Me.procesarButton.UseVisualStyleBackColor = True
        Me.procesarButton.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox4)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Location = New System.Drawing.Point(18, 280)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(914, 241)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Selección de Columnas"
        Me.GroupBox2.Visible = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.baremosDataView)
        Me.GroupBox4.Location = New System.Drawing.Point(465, 32)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(431, 184)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Fichero de parametría de baremos y cálculos"
        '
        'baremosDataView
        '
        Me.baremosDataView.AllowUserToAddRows = False
        Me.baremosDataView.AllowUserToDeleteRows = False
        Me.baremosDataView.BackgroundColor = System.Drawing.SystemColors.Window
        Me.baremosDataView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.baremosDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.baremosDataView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1})
        Me.baremosDataView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.baremosDataView.Location = New System.Drawing.Point(3, 16)
        Me.baremosDataView.Name = "baremosDataView"
        Me.baremosDataView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.baremosDataView.Size = New System.Drawing.Size(425, 165)
        Me.baremosDataView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Característica"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.reglasDataView)
        Me.GroupBox3.Location = New System.Drawing.Point(19, 32)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(431, 184)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Fichero de parametría de reglas"
        '
        'reglasDataView
        '
        Me.reglasDataView.AllowUserToAddRows = False
        Me.reglasDataView.AllowUserToDeleteRows = False
        Me.reglasDataView.BackgroundColor = System.Drawing.SystemColors.Window
        Me.reglasDataView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.reglasDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.reglasDataView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Característica})
        Me.reglasDataView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.reglasDataView.Location = New System.Drawing.Point(3, 16)
        Me.reglasDataView.Name = "reglasDataView"
        Me.reglasDataView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.reglasDataView.Size = New System.Drawing.Size(425, 165)
        Me.reglasDataView.TabIndex = 0
        '
        'Característica
        '
        Me.Característica.HeaderText = "Característica"
        Me.Característica.Name = "Característica"
        Me.Característica.ReadOnly = True
        Me.Característica.Width = 200
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.baremosHashLabel)
        Me.GroupBox1.Controls.Add(Me.reglasHashLabel)
        Me.GroupBox1.Controls.Add(Me.sheetBaremosTextBox)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.sheetReglasTextBox)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.getColumnsButton)
        Me.GroupBox1.Controls.Add(Me.baremosButton)
        Me.GroupBox1.Controls.Add(Me.reglasButton)
        Me.GroupBox1.Controls.Add(Me.baremosTextBox)
        Me.GroupBox1.Controls.Add(Me.reglasTextBox)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(18, 15)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(914, 246)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ficheros de parametría"
        '
        'baremosHashLabel
        '
        Me.baremosHashLabel.AutoSize = True
        Me.baremosHashLabel.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.baremosHashLabel.Location = New System.Drawing.Point(371, 101)
        Me.baremosHashLabel.Name = "baremosHashLabel"
        Me.baremosHashLabel.Size = New System.Drawing.Size(0, 13)
        Me.baremosHashLabel.TabIndex = 24
        '
        'reglasHashLabel
        '
        Me.reglasHashLabel.AutoSize = True
        Me.reglasHashLabel.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.reglasHashLabel.Location = New System.Drawing.Point(371, 59)
        Me.reglasHashLabel.Name = "reglasHashLabel"
        Me.reglasHashLabel.Size = New System.Drawing.Size(0, 13)
        Me.reglasHashLabel.TabIndex = 23
        '
        'sheetBaremosTextBox
        '
        Me.sheetBaremosTextBox.Location = New System.Drawing.Point(371, 164)
        Me.sheetBaremosTextBox.Name = "sheetBaremosTextBox"
        Me.sheetBaremosTextBox.Size = New System.Drawing.Size(367, 20)
        Me.sheetBaremosTextBox.TabIndex = 22
        Me.sheetBaremosTextBox.Text = "ANSI_cat_cpr_baremos"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(25, 169)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(334, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Nombre de la hoja en el fichero de parametría de baremos y cálculos:"
        '
        'sheetReglasTextBox
        '
        Me.sheetReglasTextBox.Location = New System.Drawing.Point(371, 132)
        Me.sheetReglasTextBox.Name = "sheetReglasTextBox"
        Me.sheetReglasTextBox.Size = New System.Drawing.Size(367, 20)
        Me.sheetReglasTextBox.TabIndex = 20
        Me.sheetReglasTextBox.Text = "ANSI_cat_cpr_respaldo_configura"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 137)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(272, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Nombre de la hoja en el fichero de parametría de reglas:"
        '
        'getColumnsButton
        '
        Me.getColumnsButton.Enabled = False
        Me.getColumnsButton.Location = New System.Drawing.Point(28, 205)
        Me.getColumnsButton.Name = "getColumnsButton"
        Me.getColumnsButton.Size = New System.Drawing.Size(865, 23)
        Me.getColumnsButton.TabIndex = 18
        Me.getColumnsButton.Text = "Leer columnas"
        Me.getColumnsButton.UseVisualStyleBackColor = True
        '
        'baremosButton
        '
        Me.baremosButton.Location = New System.Drawing.Point(793, 71)
        Me.baremosButton.Name = "baremosButton"
        Me.baremosButton.Size = New System.Drawing.Size(100, 23)
        Me.baremosButton.TabIndex = 17
        Me.baremosButton.Text = "Explorar..."
        Me.baremosButton.UseVisualStyleBackColor = True
        '
        'reglasButton
        '
        Me.reglasButton.Location = New System.Drawing.Point(793, 36)
        Me.reglasButton.Name = "reglasButton"
        Me.reglasButton.Size = New System.Drawing.Size(100, 23)
        Me.reglasButton.TabIndex = 16
        Me.reglasButton.Text = "Explorar..."
        Me.reglasButton.UseVisualStyleBackColor = True
        '
        'baremosTextBox
        '
        Me.baremosTextBox.Location = New System.Drawing.Point(371, 78)
        Me.baremosTextBox.Name = "baremosTextBox"
        Me.baremosTextBox.Size = New System.Drawing.Size(367, 20)
        Me.baremosTextBox.TabIndex = 15
        '
        'reglasTextBox
        '
        Me.reglasTextBox.Location = New System.Drawing.Point(371, 36)
        Me.reglasTextBox.Name = "reglasTextBox"
        Me.reglasTextBox.Size = New System.Drawing.Size(367, 20)
        Me.reglasTextBox.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(25, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(222, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Fichero de parametría de baremos y cálculos:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(25, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(160, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Fichero de parametría de reglas:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.logTextBox)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(949, 591)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Bitácora de ejecución"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'logTextBox
        '
        Me.logTextBox.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.logTextBox.Location = New System.Drawing.Point(17, 18)
        Me.logTextBox.Name = "logTextBox"
        Me.logTextBox.Size = New System.Drawing.Size(908, 550)
        Me.logTextBox.TabIndex = 0
        Me.logTextBox.Text = ""
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Button1)
        Me.TabPage3.Controls.Add(Me.GroupBox6)
        Me.TabPage3.Controls.Add(Me.GroupBox5)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(949, 591)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Opciones"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(18, 554)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(910, 26)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Guardar opciones"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.parametrosMotorDataGrid)
        Me.GroupBox6.Location = New System.Drawing.Point(18, 137)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(487, 409)
        Me.GroupBox6.TabIndex = 1
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Parámetros del motor"
        '
        'parametrosMotorDataGrid
        '
        Me.parametrosMotorDataGrid.AllowUserToAddRows = False
        Me.parametrosMotorDataGrid.AllowUserToDeleteRows = False
        Me.parametrosMotorDataGrid.BackgroundColor = System.Drawing.SystemColors.Window
        Me.parametrosMotorDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.parametrosMotorDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.parametrosMotorDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.Valor})
        Me.parametrosMotorDataGrid.Location = New System.Drawing.Point(21, 28)
        Me.parametrosMotorDataGrid.Name = "parametrosMotorDataGrid"
        Me.parametrosMotorDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.parametrosMotorDataGrid.Size = New System.Drawing.Size(443, 360)
        Me.parametrosMotorDataGrid.TabIndex = 1
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Etiqueta"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'Valor
        '
        Me.Valor.HeaderText = "Valor"
        Me.Valor.Name = "Valor"
        Me.Valor.Width = 200
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rutaPythonTextBox)
        Me.GroupBox5.Controls.Add(Me.Label1)
        Me.GroupBox5.Location = New System.Drawing.Point(18, 21)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(911, 97)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Opciones de ejecución"
        '
        'rutaPythonTextBox
        '
        Me.rutaPythonTextBox.Location = New System.Drawing.Point(146, 32)
        Me.rutaPythonTextBox.Name = "rutaPythonTextBox"
        Me.rutaPythonTextBox.Size = New System.Drawing.Size(736, 20)
        Me.rutaPythonTextBox.TabIndex = 1
        Me.rutaPythonTextBox.Text = "C:\Users\biyec\anaconda3"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(29, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Ruta de Python 3.x"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(977, 641)
        Me.Controls.Add(Me.TabControl1)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CapRiViz 0.1 by José Alberto Bonilla Vera (2021-2022)"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.baremosDataView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.reglasDataView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.parametrosMotorDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents reglasFileDialog As OpenFileDialog
    Friend WithEvents baremosFileDialog As OpenFileDialog
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents sheetBaremosTextBox As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents sheetReglasTextBox As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents getColumnsButton As Button
    Friend WithEvents baremosButton As Button
    Friend WithEvents reglasButton As Button
    Friend WithEvents baremosTextBox As TextBox
    Friend WithEvents reglasTextBox As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents reglasDataView As DataGridView
    Friend WithEvents baremosDataView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents Característica As DataGridViewTextBoxColumn
    Friend WithEvents procesarButton As Button
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents rutaPythonTextBox As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents baremosHashLabel As Label
    Friend WithEvents reglasHashLabel As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents parametrosMotorDataGrid As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents Valor As DataGridViewTextBoxColumn
    Friend WithEvents Button1 As Button
    Friend WithEvents logTextBox As RichTextBox
End Class
